package com.example.services;

import com.example.models.Note;

import java.util.List;

public interface NoteService {
    Note add(Note note);
    void update(Note note);
    List<Note> getAll();
    void delete(Note note);
    void deleteById(Long noteId);
    Note findOne(Long id);
}
