package com.example.services;

import com.example.models.Note;
import com.example.repositories.NoteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
public class NoteServiceImpl implements NoteService {

    @Autowired
    NoteRepository noteRepository;

    @Override
    public Note add(Note note) {
        return noteRepository.saveAndFlush(note);
    }

    @Override
    public void update(Note note) {
        noteRepository.saveAndFlush(note);
    }

    @Override
    public List<Note> getAll() {
        return noteRepository.findAll();
    }

    @Override
    public void delete(Note note) {
        noteRepository.delete(note);
    }

    @Override
    public void deleteById(Long noteId) {
        noteRepository.delete(noteId);
    }

    @Override
    public Note findOne(Long id) {
        return noteRepository.findOne(id);
    }
}
