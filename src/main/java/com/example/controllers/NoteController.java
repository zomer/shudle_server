package com.example.controllers;

import com.example.models.Note;
import com.example.services.NoteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/notes")
public class NoteController {

    @Autowired
    NoteService noteService;

    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public Note createNote(@RequestBody Note note) {
        return noteService.add(note);
    }

    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public List findNotes() {
        return noteService.getAll();
    }

    @RequestMapping(value = "/delete", method = RequestMethod.POST)
    public void deleteNote(@RequestBody Note note) {
        noteService.deleteById(note.getId());
    }
}
